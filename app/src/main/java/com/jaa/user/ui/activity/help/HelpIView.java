package com.jaa.user.ui.activity.help;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help help);

    void onError(Throwable e);
}
