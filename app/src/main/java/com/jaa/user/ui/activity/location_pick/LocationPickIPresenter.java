package com.jaa.user.ui.activity.location_pick;

import com.jaa.user.base.MvpPresenter;

public interface LocationPickIPresenter<V extends LocationPickIView> extends MvpPresenter<V> {
    void address();
}
