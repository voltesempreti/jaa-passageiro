package com.jaa.user.ui.fragment.searching;

import com.jaa.user.base.MvpView;

public interface SearchingIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);
}
