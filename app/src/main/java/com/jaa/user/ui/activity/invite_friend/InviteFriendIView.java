package com.jaa.user.ui.activity.invite_friend;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.User;

public interface InviteFriendIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
