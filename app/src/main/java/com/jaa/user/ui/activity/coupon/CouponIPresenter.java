package com.jaa.user.ui.activity.coupon;

import com.jaa.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
