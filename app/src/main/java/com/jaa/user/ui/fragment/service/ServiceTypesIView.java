package com.jaa.user.ui.fragment.service;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.Service;

import java.util.List;

public interface ServiceTypesIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onError(Throwable e);

    void onSuccess(Object object);
}
