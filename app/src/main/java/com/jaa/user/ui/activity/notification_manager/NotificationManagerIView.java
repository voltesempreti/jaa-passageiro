package com.jaa.user.ui.activity.notification_manager;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> notificationManager);

    void onError(Throwable e);

}