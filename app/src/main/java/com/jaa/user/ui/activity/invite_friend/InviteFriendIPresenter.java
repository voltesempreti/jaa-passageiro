package com.jaa.user.ui.activity.invite_friend;

import com.jaa.user.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
