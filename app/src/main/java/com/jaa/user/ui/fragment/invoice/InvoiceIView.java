package com.jaa.user.ui.fragment.invoice;

import com.appoets.paytmpayment.PaytmObject;
import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.BrainTreeResponse;
import com.jaa.user.data.network.model.CheckSumData;
import com.jaa.user.data.network.model.Message;

public interface InvoiceIView extends MvpView {
    void onSuccess(Message message);

    void onSuccess(Object o);

    void onSuccessPayment(Object o);

    void onError(Throwable e);

    void onSuccess(BrainTreeResponse response);

    void onPayumoneyCheckSumSucess(CheckSumData checkSumData);

    void onPayTmCheckSumSucess(PaytmObject payTmResponse);
}
