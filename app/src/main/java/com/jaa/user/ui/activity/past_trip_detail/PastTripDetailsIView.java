package com.jaa.user.ui.activity.past_trip_detail;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.Datum;

import java.util.List;

public interface PastTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> pastTripDetails);

    void onError(Throwable e);
}
