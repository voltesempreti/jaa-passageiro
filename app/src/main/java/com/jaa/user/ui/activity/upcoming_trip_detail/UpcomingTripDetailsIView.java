package com.jaa.user.ui.activity.upcoming_trip_detail;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);

    void onError(Throwable e);
}
