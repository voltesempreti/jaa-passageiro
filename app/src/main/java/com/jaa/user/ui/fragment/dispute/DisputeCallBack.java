package com.jaa.user.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
