package com.jaa.user.ui.activity.coupon;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);

    void onError(Throwable e);
}
