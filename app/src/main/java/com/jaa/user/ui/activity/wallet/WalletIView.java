package com.jaa.user.ui.activity.wallet;

import com.appoets.paytmpayment.PaytmObject;
import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.AddWallet;
import com.jaa.user.data.network.model.BrainTreeResponse;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);

    void onSuccess(PaytmObject object);

    void onSuccess(BrainTreeResponse response);
    void onError(Throwable e);
}
