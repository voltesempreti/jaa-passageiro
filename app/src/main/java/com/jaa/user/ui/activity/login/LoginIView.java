package com.jaa.user.ui.activity.login;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.ForgotResponse;
import com.jaa.user.data.network.model.Token;

public interface LoginIView extends MvpView {
    void onSuccess(Token token);

    void onSuccess(ForgotResponse object);

    void onError(Throwable e);
}
