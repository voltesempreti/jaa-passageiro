package com.jaa.user.ui.activity.passbook;

import com.jaa.user.base.MvpView;
import com.jaa.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);

    void onError(Throwable e);
}
